package br.com.avaliacaojava.util;

/**
 * 
 * Classe responsável por fornecer as contantes do sistema.
 *
 * @since 14 de jun de 2016 01:42:27
 * @author Gustavo de Camargo <gustavo_umbr@hotmail.com>
 */
public final class Constantes{
	public static final String DRIBBBLE_SHOT_API = "https://api.dribbble.com/v1/shots";
	public static final String ACCESS_TOKEN = "ddafe4922e43509abe8bf57d20f431d6ceb052c3f70c49be9b1957b27f14ebde";
}
