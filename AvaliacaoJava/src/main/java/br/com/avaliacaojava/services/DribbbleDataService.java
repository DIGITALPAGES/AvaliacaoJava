package br.com.avaliacaojava.services;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import br.com.avaliacaojava.services.abstractservice.AbstractDataService;
import br.com.avaliacaojava.util.Constantes;

@RestController
@RequestMapping( "/dribbble" )
public class DribbbleDataService extends AbstractDataService{
	private static final Logger LOGGER = LogManager.getLogger( DribbbleDataService.class );

	@RequestMapping( value = "listar/{page}", method = { RequestMethod.GET } )
	public ResponseEntity< Object > list( @PathVariable( ) Integer page ) {
		if ( page == null || page <= 0 ) {
			return new ResponseEntity< Object >( getMessage( "mensagem.erro.badRequest", "page" ), HttpStatus.BAD_REQUEST );
		}

		LOGGER.info( "<< Inicializando a listagem de shots. >>" );

		// Motando a URL.
		StringBuilder url = new StringBuilder();
		url.append( Constantes.DRIBBBLE_SHOT_API );
		url.append( "?access_token=" );
		url.append( Constantes.ACCESS_TOKEN );
		url.append( "&sort=views" );
		url.append( "&page=" );
		url.append( page );

		// Realizando a requisicao na API.
		String response = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			response = restTemplate.getForObject( url.toString(), String.class );
		} catch ( Exception e ) {
			if ( e instanceof ResourceAccessException ) {
				return new ResponseEntity< Object >( getMessage( "mensagem.erro.semconexao" ), HttpStatus.REQUEST_TIMEOUT );
			}
		}

		LOGGER.info( "<< Listagem realizada com sucesso. >>" );
		return new ResponseEntity< Object >( response, HttpStatus.OK );
	}

	@RequestMapping( value = "obter/{id}", method = { RequestMethod.GET } )
	public ResponseEntity< Object > obter( @PathVariable( ) Integer id ) {
		if ( id == null || id <= 0 ) {
			return new ResponseEntity< Object >( getMessage( "mensagem.erro.badRequest", "id" ), HttpStatus.BAD_REQUEST );
		}

		LOGGER.info( "<< Inicializando a consulta do shot. >>" );

		// Motando a URL.
		StringBuilder url = new StringBuilder();
		url.append( Constantes.DRIBBBLE_SHOT_API );
		url.append( "/" + id );
		url.append( "?access_token=" );
		url.append( Constantes.ACCESS_TOKEN );

		// Realizando a requisicao na API.
		String response = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			response = restTemplate.getForObject( url.toString(), String.class );
		} catch ( Exception e ) {
			if ( e instanceof ResourceAccessException ) {
				return new ResponseEntity< Object >( getMessage( "mensagem.erro.semconexao" ), HttpStatus.REQUEST_TIMEOUT );
			}
		}

		LOGGER.info( "<< Shot consultado com sucesso. >>" );
		return new ResponseEntity< Object >( response, HttpStatus.OK );
	}

}