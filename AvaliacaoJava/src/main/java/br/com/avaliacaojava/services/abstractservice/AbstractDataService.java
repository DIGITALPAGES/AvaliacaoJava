package br.com.avaliacaojava.services.abstractservice;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

public abstract class AbstractDataService{

	/**
	 * 
	 * Método responsável por fornecer a mensagem do arquivo properties.
	 *
	 * @since 14 de jun de 2016 01:42:09
	 * @author Gustavo de Camargo <gustavo_umbr@hotmail.com>
	 * @param refMensagem
	 * @param param
	 * @return
	 */
	public String getMessage( String refMensagem, Object... param ) {
		try {
			Properties properties = new Properties();
			InputStream resourceStream = Thread.currentThread().getContextClassLoader().getResourceAsStream( "ApplicationResources.properties" );
			properties.load( resourceStream );

			if ( param != null ) {
				return MessageFormat.format( properties.getProperty( refMensagem ), param );
			} else {
				return properties.getProperty( refMensagem );
			}
		} catch ( IOException e ) {
			e.printStackTrace();
			return null;
		}
	}

}
