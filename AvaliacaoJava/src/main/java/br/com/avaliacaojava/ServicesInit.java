package br.com.avaliacaojava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan( "br.com.avaliacaojava" )
public class ServicesInit{

	/**
	 * 
	 * M�todo principal respons�vel por inicializar a aplica��o.
	 *
	 * @since 14 de jun de 2016 01:42:44
	 * @author Gustavo de Camargo <gustavo_umbr@hotmail.com>
	 * @param args
	 */
	public static void main( String[ ] args ) {
		SpringApplication.run( ServicesInit.class, args );
	}

}
