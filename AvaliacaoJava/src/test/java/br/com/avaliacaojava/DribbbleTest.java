package br.com.avaliacaojava;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import br.com.avaliacaojava.util.Constantes; 

/**
 * 
 * Classe responsável por conter os testes unitários.
 *
 * @since 14 de jun de 2016 02:11:18
 * @author Gustavo de Camargo <gustavo_umbr@hotmail.com>
 */
public final class DribbbleTest{

	@Ignore
	public void listar() {
		final Integer PAGE = 1;

		// Motando a URL.
		StringBuilder url = new StringBuilder();
		url.append( Constantes.DRIBBBLE_SHOT_API );
		url.append( "?access_token=" );
		url.append( Constantes.ACCESS_TOKEN );
		url.append( "&sort=views" );
		url.append( "&page=" );
		url.append( PAGE );

		RestTemplate restTemplate = new RestTemplate();
		String response = restTemplate.getForObject( url.toString(), String.class );

		Assert.assertNotNull( response );
	}

	@Test
	public void obter() {
		final Integer ID = 2774455;

		// Motando a URL.
		StringBuilder url = new StringBuilder();
		url.append( Constantes.DRIBBBLE_SHOT_API );
		url.append( "/" + ID );
		url.append( "?access_token=" );
		url.append( Constantes.ACCESS_TOKEN );

		RestTemplate restTemplate = new RestTemplate();
		String response = restTemplate.getForObject( url.toString(), String.class );

		Assert.assertNotNull( response );
	}

}
